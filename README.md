# Load Balancer POC


## The main idea
This is a Proof of concept for the load balancer feature on codectome. The idea for this is project is to clone the infraestructure between two machines that are on the same LAN in order to have redundancy and enough infraestructure for the use in bigger interviewing process.

## The implementation
The idea is to have two machines, one having the whole infraestructure of codectome and other having the code-core, code challenge and the execution/compiltation bundle, wich includes Code-Compilation, code-execution as well as code package. 

![alt text](https://gitlab.com/FrankGimenez/load-balancer-poc/-/raw/99c85473caaa03d791d73266f66baa7561e65e79/image.png)

once this architecture is complete, on the gateway's Ocelot you can add the endpoints that you want to be managed with a load balancer adding multiple urls, something like this

```json
{
    "DownstreamPathTemplate": "/api/Product",
    "DownstreamScheme": "http",
    "DownstreamHostAndPorts": [
        {
        "Host": "localhost",
        "Port": 6002
        },
        {
        "Host": "localhost",
        "Port": 6003
        }
    ],
    "UpstreamPathTemplate": "/GetAllProducts",
    "LoadBalancerOptions": {
        "Type": "LeastConnection"
    },
    "UpstreamHttpMethod": [ "Get" ]
}
```
Note that you can add multiple Host & port values but if you don't use the Load Balancer Options object later on, this will not work. 


## The Challenge:
During our time trying to implement the load balancer on the real codectome app, we encountered various issues:
- The machines weren't able to comunicate being on different networks. 
    - In spite of having both machines on the same LAN, we couldn't manage to make  the both comunicate. 
- Implementing dual deploy CI/CD
    - We didin't had the skills to make the simultaneous deploy on both machines using gitlab ci/cd, so to try to implement it as a last resource we just cloned and started the microservices on the machines manually.


### Sources: 
I personally recomend you to read some of this tutorials and documentation to learn how ocelot load balancing works.
- [Load Balancer - Official Ocelot Documentation](https://ocelot.readthedocs.io/en/latest/features/loadbalancer.html)
- [Load Balancer tutorial - C# Corner](https://www.c-sharpcorner.com/article/building-api-gateway-using-ocelot-in-asp-net-core-load-balancing/)
- [Video tutorial on how to make a load balancer](https://youtu.be/dQw4w9WgXcQ)
