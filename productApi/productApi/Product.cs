using Microsoft.AspNetCore.Routing.Matching;

namespace productApi
{
    public class Product
    {
        public Product(int id, string name, string type, int price)
        {
            Id = id;
            Name = name;
            Type = type;
            Price = price;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public int Price { get; set; }
        
    }
}